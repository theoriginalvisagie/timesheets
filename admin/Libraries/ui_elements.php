<?php
/**
 * Public functions to display generic UI elements
 */
  
  /**
   * @param string $table table it needs to display columns from
   * @param string $modalName name for modal
   * @param string $title title that needs to be displayed on modal
   * @param string $body body of modal
   * @param string $type POST/AJAX
   * @param string $javaScript custom javascript to execute on click of save
   * @param string $style custom style for modal toggle button
   */
  function createModel($table, $modalName, $title, $body, $type, $javaScript="",$style=""){
      echo "<button type='button' class='btn btn-primary' data-bs-toggle='modal' data-bs-target='#$modalName' style='$style'>
              $title
            </button>";
        
      if($type == "post"){
          echo "<form method='post'>";
      }else if($type == "ajax"){
        echo "<form id='form_$modalName'>";
        
      }
        echo"
        <div class='modal fade' id='$modalName' tabindex='-1' aria-labelledby='exampleModalLabel' aria-hidden='true'>
          <div class='modal-dialog'>
            <div class='modal-content'>
              <div class='modal-header'>
                <h5 class='modal-title' id='exampleModalLabel'>$title</h5>
                <button type='button' class='btn-close' data-bs-dismiss='modal' aria-label='Close'></button>
              </div>
              <div class='modal-body'>
                $body
              </div>
              <div class='modal-footer'>";
                
                if($type == "post"){
                  echo "<input type='submit' name='addNew' id='addNew' value='Save' class='btn btn-primary'>";
                  echo "<input type='hidden' name='table' id='table' value='$table'>";
                }else if($type == "ajax"){
                  echo "<button type='button' class='btn btn-primary' onclick='$javaScript' data-bs-dismiss='modal'>Save</button>";
                }
                echo"<button type='button' class='btn btn-secondary' data-bs-dismiss='modal'>Close</button>";
              echo" </div>
            </div>
          </div>
        </div>";
        echo "</form>";
      
  }
?>