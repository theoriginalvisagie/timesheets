<nav class="navbar navbar-expand-lg navbar-light bg-light">
<div class="container">
  <a class="navbar-brand" href="#">TimeSheets</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent" >
    <ul class="navbar-nav mr-auto" >
        <?php
            if(isset($_SESSION['userID']) && $_SESSION['userID'] != ""){
                echo "<li class='nav-item'>
                            <a class='nav-link' href='".URLROOT."/admin/Modules/Clients/index.php'>Clients</span></a>
                        </li>
                        <li class='nav-item'>
                            <a class='nav-link' href='".URLROOT."/admin/Modules/Reports/index.php'>Reports</span></a>
                        </li>";
            }
        ?>
    </ul>
    
  </div>
  <?php if(!empty($_SESSION)) : ?>
    <form method='post'>
      <button class='btn'><i><?php echo str_replace("_"," ",$_SESSION['userName']);?></i></button>
      <input type='submit' style='float:right;' class='btn btn-outline-dark btn-light' value='Logout' id='logOut' name='logOut'>
    </form>
  <?php endif ;?>
  </div>
</nav>

<?php
  if(isset($_POST['logOut']) && $_POST['logOut']!=""){
    foreach($_SESSION as $s){
      unset($s);
      session_destroy();

      header("Location: ". URLROOT."/login.php");
    }
  }
?>