<?php
    session_start();
    require_once(ADMIN_DIR."/Libraries/dbConnect.php");
    require_once(ADMIN_DIR."/Libraries/ui_elements.php");

    /**
     * Class to display, add and update clients
     * Display, update and add time logs per client
     */
    class Clients {
        private $userID;
        function __construct(){
            $this->userID = $_SESSION['userID'];
        }

        function init(){
            if(isset($_POST['viewClient']) && $_POST['viewClient']!="" && $_POST['clientID']!="clientID"){
                $this->viewClient($_POST['clientID']);
            }else{
                echo "<div id='displayAllClients'>";
                $this->displayClients($this->userID);
                echo "</div>";
            }
            
        }

        function displayClients($userID){
            echo "<h1 style='text-align:center; margin:40px;'>Clients</h1>";
            $this->addClients();
            echo "<br>";
            $sql = "SELECT * FROM clients WHERE userID = '$userID'";

            $results = exeSQL($sql);
            $cnt = 1;
            echo "<br><table style='width: 100%'>";
            echo "<tr>";
            foreach($results as $result){
                echo "<td style='width: 50%'><div class='card' >
                        <div class='card-body'>
                        <form method='post'>
                            <h5 class='card-title'>{$result['company']}</h5>
                            <p class='card-text'>
                                {$result['primary_name']} {$result['primary_surname']} | {$result['location']} 
                                <input type='submit' name='viewClient' id='viewClient' class='btn btn-info' style='float:right;' value='View'>
                                <input type='hidden' name='clientID' id='clientID' value='{$result['id']}'>
                            </form>
                        </p>
                        </div>
                    </div></td>";
                if($cnt%2 == 0){
                    echo "</tr>";
                }
                $cnt ++;
            }
            echo "</tr>";
            echo "</table>";

        }

        function viewClient($clientID){
            $sql = "SELECT * FROM clients WHERE id = '$clientID'";
            $results = exeSQL($sql);

            echo "<h1 style='text-align:center; margin:40px;'>{$results[0]['company']}</h1>";

            echo "<div class='row'>
                    <div class='col-6'>
                        <center>
                            <button id='clientDetailbutton' onclick='toggleDivs(\"clientDetail\");'>Details</button>
                            <hr id='clientDetailHR'>
                        </center>
                    </div>
                    <div class='col-6'>
                        <center>
                            <button id='clientTimesheetbutton' onclick='toggleDivs(\"clientTimesheet\");'>Timesheets</button>
                            <hr id='clientTimesheetHR'>
                        </center>
                    </div>
                    <div class='alert alert-success' style='display:none;' id='updateSuccesss'></div>
                </div>";
            echo "<div class='row'>
                    <div id='clientDetail' style='display:block;'>";
                    echo $this->getClientDetails($results);
                    echo "</div>
                    

                    <div id='clientTimesheet' style='display:none;'>";
                    echo $this->getClientTimeSheet($clientID);
                    echo"</div>
                    
                </div>";
        }

        function getClientDetails($client){
            $clientID = $client[0]['id'];
            $clientFields = getTableColumns("clients","id,userID,dateCreated");
            $cnt = 0;
            echo "<form id='clientDetails'>
                    <table style='width:100%;' id='updateClientData'>";
            echo "<tr>";
            foreach($clientFields as $field){
                $placeholder = ucwords(str_replace("_"," ",$field['Column']));
                $value = $client[0][$field['Column']];

                if(strpos($value,"%20")!==false){
                    $value = str_replace("%20"," ",$value);
                }else if(strpos($value,"%40")!==false){
                    $value = str_replace("%40","@",$value);
                }   

                echo "<td>
                    <label for='{$field['Column']}'><strong>$placeholder:</strong></label>
                    <input class='form-control type='text' value='$value' name='{$field['Column']}' id='{$field['Column']}' placeholder='$placeholder'>
                    </td>";
                
                $cnt ++;
                if($cnt%2 == 0){
                    echo "</tr>";
                }
            }
            echo "</tr>";
            echo "</table></form>
                  <br>
                  <button class='btn btn-primary' name='updateClient' id='updateClient' style='width:100%;' onclick='updateClient(\"{$_SESSION['userID']}\",\"$clientID\")' >Save Changes</button>";

        }

        function getClientTimeSheet($id){
            $client = getColumnValues("clients","company","id='$id'");
            $timeSheetColumns = getTableColumns("timesheets","id,dateCreated");

            $s = "<form id='addClientLog_{$id}' method='post'>
                    <table class='table table-striped'>";
            foreach($timeSheetColumns as $columns){

                $column = ucwords(str_replace("_"," ",$columns['Column']));
                if($column == "ClientID"){
                    $column = "Company";
                }else if($column == "Total"){
                    $column = "Hours";
                }
                $s .= "<tr>";
                $s .= "<td>$column:</td>";
                $s .= "<td>";
                if($columns['Column'] == "clientID"){
                    $s .= "<input class='form-control type='text' value='{$client[0]['company']}' disabled>
                               <input type='hidden' name='{$columns['Column']}' id='{$columns['Column']}' value='$id'>";
                }else if($columns['Column'] == "date"){
                    $s .= "<input type='date' name='{$columns['Column']}' id='{$columns['Column']}'>";
                }else if($columns['Type'] == "mediumtext"){
                    $s .= "<textarea name='{$columns['Column']}' id='{$columns['Column']}' rows='5' cols='45'></textarea>";
                }else{
                    $s .= "<input class='form-control type='text' name='{$columns['Column']}' id='{$columns['Column']}'>";
                }
                $s .= "</td>";
                $s .= "</tr>";
            }
            $s .= "</table>
                    </form>";

            echo createModel("timesheets","addClientLog_$id","Add Log", $s,"ajax","saveClientLog($id)","width:100%;");

            echo "<div id='clientLogTable_$id'>";
            $this->displayLogDataTable($id);
            echo "</div>";
        }

        function displayLogDataTable($clientID){
            $timeSheetColumns = getTableColumns("timesheets","id,clientID,comments,dateCreated");
            $sql = "SELECT * FROM timesheets 
                    WHERE clientID='$clientID' ORDER BY dateCreated DESC";
            $result = exeSQL($sql);
            echo "<table class='table table-striped'>";
            echo "<thead>";
            echo "<th>#</th>";
            foreach($timeSheetColumns as $columns){
                $name = ucwords(str_replace("_"," ",$columns['Column']));
                echo "<th>$name</th>";
            }  
            echo "<th>Action</th>";
            echo "</thead>";
            $cnt = 1;
            foreach($result as $r){
                echo "<tr>";
                echo "<td>$cnt</td>";
                foreach($timeSheetColumns as $columns){
                    echo "<td>{$r[$columns['Column']]}</td>";
                }

                $s = $this->getLogDetails($r);
                echo "<td>";
                echo createModel("timesheets","viewLog_{$r['id']}","View", $s,"","","");
                echo "</td>";

                echo "</tr>";
                $cnt++;
            }

            echo "</table>";
        }   

        function getLogDetails($data){
            $s = "<pre>".print_r($data,true)."</pre>";
            $s = "<table width='100%'>
                    <tr>
                        <td><strong><i>Logged On:</i></strong></td>
                        <td>{$data['date']}</td>
                        <td><strong><i>Total:</i></strong></td>
                        <td>{$data['total']}</td>
                    </tr>
                    <tr>
                        <td colspan='4'><strong><i>Comments:</i></strong></td>
                    </tr>
                    <tr>
                        <td colspan='4'>".str_replace("%20"," ",$data['comments'])."</td>
                    </tr>
                </table>";
            return $s;
        }

        function addClients(){
            $headings = getTableColumns("clients","id,userID,dateCreated");
            $s = "<table class='table table-striped'>";
            foreach($headings as $key=>$value){
                $name = ucwords(str_replace("_"," ",$key));
                $s .= "<tr>";
                $s .= "<td>$name:</td>";
                $s .= "<td><input class='form-control type='text' name='{$value['Column']}' id='{$value['Column']}'></td>";
                $s .= "</tr>";
            }
            $s .= "</table>";

            return createModel("clients","addNewClient","Add New Client", $s, "ajax", "addNewClient(\"addNewClient\")" ,"float:right;");
        }
    }
?>