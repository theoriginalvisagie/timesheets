function addNewClient(formName){
    var formData = $('#form_'+formName).serialize();

    $.ajax({
        url: rootDirectory + "/Clients/ajax/clientAjax.php",
        type: 'post',
        data: ({'action':"addNewClientData",data:formData}),
        dataType: "html",
        success: function (html) {
            document.getElementById('displayAllClients').innerHTML = html;
        }
    });
}

function updateClient(userID,clientID) {
    var formData = $('#clientDetails').serialize();
    const data = formData.split("&");
    $.ajax({
        url: rootDirectory + "/Clients/ajax/clientAjax.php",
        type: 'post',
        data: ({'action':"updateClientDetails",data:data,userID:userID,clientID:clientID}),
        success: function (response) {
            console.log(response);
            if(response == "updated"){
                document.getElementById('updateSuccesss').style.display = "block";
                document.getElementById('updateSuccesss').innerHTML = "Client Details Updated."
                setTimeout(function(){document.getElementById("updateSuccesss").style.display="none";},1500);
            }
        }
    });
}

function toggleDivs(show){
    var details = document.getElementById("clientDetail");
    var timesheet = document.getElementById("clientTimesheet");
    var timeHR = document.getElementById("clientTimesheetHR")
    var timeButton = document.getElementById("clientTimesheetbutton")
    var clientButton = document.getElementById("clientDetailbutton")
    var clientHR = document.getElementById("clientDetailHR")

    if(show == "clientDetail"){
        timeHR.style.color = "grey";
        timeButton.style.color = "grey";
        clientButton.style.color = "#6bafdb";
        clientHR.style.color = "#6bafdb";

        details.style.display = "block";
        timesheet.style.display = "none";
    }else if(show == "clientTimesheet"){
        timeHR.style.color = "#6bafdb";
        timeButton.style.color = "#6bafdb";
        clientButton.style.color = "grey";
        clientHR.style.color = "grey";

        details.style.display = "none";
        timesheet.style.display = "block";
    }

}

function saveClientLog(clientID){
    var formData = $('#form_addClientLog_'+clientID).serialize();

    $.ajax({
        url: rootDirectory + "/Clients/ajax/clientAjax.php",
        type: 'post',
        data: ({'action':"saveClientLogData",formData:formData,clientID:clientID}),
        dataType: "html",
        success: function (html) {
            document.getElementById("clientLogTable_"+clientID).innerHTML = html;
            // document.getElementById("addClientLog_"+clientID).reset();
        }
    });
}
