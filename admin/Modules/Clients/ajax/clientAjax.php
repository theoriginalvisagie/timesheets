<?php
/**
 * Ajax calls to Add and update new client as well as save time log data.
 */
    session_start();
    require_once(dirname(__FILE__,4)."/config.php");   
    require_once(ADMIN_DIR."/Libraries/dbConnect.php");   
    require_once(ADMIN_DIR."/Modules/Clients/Clients_class.php"); 

    if($_POST['action'] == "updateClientDetails"){
        $columns = "userID,";
        $values = "'".$_POST['userID']."',";

        $sql = "UPDATE clients SET userID = '{$_POST['userID']}', ";
        foreach($_POST['data'] as $data=>$value){
            $sql .= substr($value,0,strpos($value,"="));
            $sql .= " = ";
            $sql .= "'".str_replace("%20"," ",substr($value,strpos($value,"=")+1))."',";
        }

        $sql = rtrim($sql,",");
        $sql .= " WHERE id='{$_POST['clientID']}'";

        $res = exeSQL($sql);
        
        if($res){
            echo "updated";
        }else{
            echo "failed";
        }

    }else if($_POST['action'] == "saveClientLogData"){
        $client = new Clients();
        $data = explode("&",$_POST['formData']);

        $sql = "INSERT INTO timesheets(";
        foreach($data as $d){
            $sql .= substr($d,0,strpos($d,"=",0)).",";
        }
        $sql = rtrim($sql,",").") VALUES (";

        foreach($data as $d){
            $sql .= "'".substr($d,strpos($d,"=",0)+1)."',";
        }

        $sql = rtrim($sql,",").")";
        // echo $sql;
        $res = exeSQL($sql);

        if($res){
            $client->displayLogDataTable($_POST['clientID']);
        }else{
            echo "failed";
        }
    }else if($_POST['action'] == "addNewClientData"){
        $client = new Clients();
        $data = explode("&",$_POST['data']);

        $sql = "INSERT INTO clients(userID,";
        foreach($data as $d){
            $sql .= substr($d,0,strpos($d,"=",0)).",";
        }
        $sql = rtrim($sql,",").") VALUES ( '{$_SESSION['userID']}', ";

        foreach($data as $d){
            $sql .= "'".substr($d,strpos($d,"=",0)+1)."',";
        }

        $sql = rtrim($sql,",").")";

        $res = exeSQL($sql);

        if($res){
            $client->displayClients($_SESSION['userID']);
        }else{
            echo "failed";
        }
            
    }
?>