let rootDirectory = "http://localhost/timesheets/admin/Modules";

function validateLogIn(){
    var password = document.getElementById("userPassword").value;
    var email = document.getElementById("userEmail").value;

    $.ajax({
        url: rootDirectory + "/Users/ajax/validateAjax.php",
        type: 'post',
        data: ({'action':"validateUserLogIn",email:email, password:password}),
        success: function (response) {
            if(response == "incorrect_password"){
                document.getElementById("pass_err").style.display = "block";
            }else if(response == "no_user_found"){
                document.getElementById("email_err").style.display = "block";
            }else if(response == "valid"){
                window.location.href = rootDirectory + "/Clients/index.php";
            }
        }
    });

}

function registerNewUser(){    
    var data = $('#registerUserForm').serialize();
    data = data.replace("%40","@");

    $.ajax({
        url: rootDirectory + "/Users/ajax/validateAjax.php",
        type: 'post',
        data: ({'action':"registerNewUser",data:data}),
        success: function (response) {
            console.log(response);
            if(response == "New User Created"){
                alert("Account Created, please log in");
                window.location.href = "http://localhost/TimeSheetsV1/login.php";
            }
        }
    });
}

function confirmPasswordMatch(){
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirmPassword").value;

    if(password === confirmPassword){
        document.getElementById("confirmPasswordLabel").style.display = "none";
        return true;
    }else{
        document.getElementById("confirmPasswordLabel").style.display = "block";
        return false;
    }
}

function checkEmail(email){
    $.ajax({
        url: rootDirectory + "/Users/ajax/validateAjax.php",
        type: 'post',
        data: ({'action':"checkEmail",email:email}),
        success: function (response) {
            if(response == "true"){
                document.getElementById("email").style.color = "red";
                document.getElementById("checkEmailExists").style.display = "block";
            }else{
                document.getElementById("checkEmailExists").style.display = "none";
                document.getElementById("email").style.color = "black";
            }
        }
    });
}

function checkStrength(password){
    var checkSpecial = /[*@!#%&()^~{}]+/.test(password),
        checkUpper = /[A-Z]+/.test(password),
        checkNumber = /[0-9]+/.test(password);

    if(checkSpecial){
        document.getElementById("specialChar").style.color = "green";
        document.getElementById("specialChar").className = "fa fa-check";
    }else{
        document.getElementById("specialChar").style.color = "red";
        document.getElementById("specialChar").className = "fa fa-times";
    }

    if(checkUpper){
        document.getElementById("upperCase").style.color = "green";
        document.getElementById("upperCase").className = "fa fa-check";
    }else{
        document.getElementById("upperCase").style.color = "red";
        document.getElementById("upperCase").className = "fa fa-times"; 
    }

    if(checkNumber){
        document.getElementById("numberCount").style.color = "green";
        document.getElementById("numberCount").className = "fa fa-check";
    }else{
        document.getElementById("numberCount").style.color = "red";
        document.getElementById("numberCount").className = "fa fa-times";
    }

    if(password.length>=8){
        document.getElementById("numberChar").style.color = "green";
        document.getElementById("numberChar").className = "fa fa-check";
    }else{
        document.getElementById("numberChar").style.color = "red";
        document.getElementById("numberChar").className = "fa fa-times";
    }

    if(checkSpecial && checkUpper && checkNumber && password.length>=8){
        return true;
    }else{
        document.getElementById("passwordStrength").style.display = "block";
    }
}

function userLoginError(){
    document.getElementById("logInError").style.display="block";
}