<?php
    require_once(ADMIN_DIR."/config.php");   
    require_once(ADMIN_DIR."/Libraries/dbConnect.php");   

    class User{
        function __construct(){

        }

        function init(){
            if(isset($_POST['registernewUser']) && $_POST['registernewUser'] != ""){
                $this->displayRegister();
            }else if(isset($_POST['loginUser']) && $_POST['loginUser'] != ""){
                $this->displayLogIn();
            }else{
                $this->displayLogIn();
            }
        }

        function displayLogIn(){
            echo "<h1>Welcome</h1>
                    <div class='alert alert-success' style='display:none' id='successRefMsg'>Welocome, you may log in.</div>
                    <p>Please fill in your details to log into TimeSheets.</p>";

            echo "<form method='post'>";
            echo "<div class='form-group'>
                    <label for='userEmail'>Email: <sup>*</sup></label>
                    <input class='form-control form-control-lg' type='email' name='userEmail' id='userEmail'>
                    <div id='email_err' style='display:none; color:red;'>No User Found</div>
                </div>
                <div class='form-group'>
                    <label for='userEmail'>Password: <sup>*</sup></label>
                    <input class='form-control form-control-lg' type='password' name='userPassword' id='userPassword' >
                    <div id='pass_err' style='display:none; color:red;'>Incorrect Password</div>
                </div>
                <br>
                <div class='row'>
                    <div class='col'>
                        <input type='button' value='Login' name='validateUserLogIn' id='validateUserLogIn' class='btn btn-success btn-block' onclick='validateLogIn()'>
                    </div>
                    <div class='col'>
                        <input type='submit' name='registernewUser' id='registernewUser' value='No account? Register' class='btn btn-light btn-block' style='float:right;'>
                    </div>
                </div>";
                 
            echo "</form>";
        }

        function displayRegister(){
            
            $headings = getTableColumns("users","id,dateCreated");
            echo "<h1>Welcome</h1>
                    <p>Please fill in your details register a new account.</p>";
            echo "<form method='post' id='registerUserForm'>";
           
            foreach($headings as $name=>$data){
                $name = ucwords(str_replace("_"," ",$data['Column']));
                if($data['Column']=="email"){
                    $type = "email";
                    $js = "onkeyup='checkEmail(this.value)'";
                    $errLabel = "<label style='display:none; color:red;' name='checkEmailExists' id='checkEmailExists'>Email all ready in use</label>";
                }else if($data['Column']=="password"){
                    $type = "password";
                    $js = "onkeyup='checkStrength(this.value)'";
                    $passCheck = "<div style='display:none;' name='passwordStrength' id='passwordStrength'>
                            <p style='font-size:10px; margin-bottom:0;'>Password must contain at least:</p>
                            <ul style='font-size:10px; list-style:none'>                                               
                                <li><i class='fa fa-times' style='color:red;' id='upperCase'></i> One Uppercase</li>
                                <li><i class='fa fa-times' style='color:red;' id='numberCount'></i> One Number</li>
                                <li><i class='fa fa-times' style='color:red;' id='specialChar'></i> One Special Character</li>
                                <li><i class='fa fa-times' style='color:red;' id='numberChar'></i> 8 Characters</li>
                            </ul>
                        </div>";
                }else{
                    $type = "text";
                }

                echo "<div class='form-group'>
                        <label for='{$data['Column']}'>$name: <sup>*</sup></label>
                        <input class='form-control form-control-lg' type='$type' name='{$data['Column']}' id='{$data['Column']}' $js>
                        $errLabel
                        $passCheck
                    </div>";
            }

            echo "<div class='form-group'>
                        <label for='confirmPassword'>Confirm Password: <sup>*</sup></label>
                        <input class='form-control form-control-lg' type='password' name='confirmPassword' id='confirmPassword' onkeyup='confirmPasswordMatch()'>
                        <label style='display:none; color:red;' name='confirmPasswordLabel' id='confirmPasswordLabel'>Passwords don't match</label>

                    </div>";

            echo "<br><div class='row'>
                     <div class='col'>
                         <input type='button' id='registerUser' name='registerUser' value='Register' class='btn btn-success btn-block' onclick='registerNewUser()'>
                     </div>
                     <div class='col'>
                        <input type='submit' name='loginUser' id='loginUser' value='Have an account? Login' class='btn btn-light btn-block' style='float:right;'>
                     </div>
                </div>";
            echo "</form>";
        }

    }
?>