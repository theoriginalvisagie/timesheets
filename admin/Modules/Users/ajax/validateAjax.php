<?php
    require_once(dirname(__FILE__,4)."/config.php");   
    require_once(ADMIN_DIR."/Libraries/dbConnect.php");   

    if($_POST['action'] == "validateUserLogIn"){
        $sql = "SELECT * FROM users WHERE email = '{$_POST['email']}'";

        $result = exeSQL($sql);
        if($result){
            if($result[0]['email'] == $_POST['email'] && password_verify($_POST['password'], $result[0]['password'])){
                $return = "valid";
                session_start();
                $_SESSION['userID'] = $result[0]['id'];
                $_SESSION['userName'] = $result[0]['name']."_".$result[0]['surname'];
                $_SESSION['time_logged_in'] = date('Y-m-d H:i:s');
                $_SESSION['loggedIn'] = "Yes";

            }else if($result[0]['password'] != $_POST['password']){
                $return = "incorrect_password";
            }
        }else{
            $return = "no_user_found";
        }

        echo $return;
    }else if($_POST['action'] == "checkEmail"){
        $email = strtolower($_POST['email']);
        $sql = "SELECT * FROM users WHERE email='$email'";

        $response = exeSQL($sql);

        if($response){
            echo "true";
        }else{
            echo "false";
        }
    }else if($_POST['action']=="registerNewUser"){
        $columns = "";
        $data = "";
        $userInforamtion = explode("&",$_POST['data']);

        foreach($userInforamtion as $key=>$value){
            $newKey = substr($value,0,strpos($value,"="));
            $newValue = substr($value,strpos($value,"=")+1);
            $userInforamtion[$newKey] = $newValue;
            unset($userInforamtion[$key]);
        }

        unset($userInforamtion["confirmPassword"]);

        foreach($userInforamtion as $key=>$value){
            $columns .= $key.",";
            
            if($key=="emial"){
                $value = strtolower($value);
            }else if($key=="password"){
                $value = password_hash($value, PASSWORD_DEFAULT);
            }

            $data .= "'".$value."',";
        }
        $columns = rtrim($columns,",");
        $data = rtrim($data,",");
        $sql = "INSERT INTO users($columns) VALUES($data)";

        $response = exeSQL($sql);

        if($response){
            echo "New User Created";
        }else{
            echo "Error";
        }
    }
?>