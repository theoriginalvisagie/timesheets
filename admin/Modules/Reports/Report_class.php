<?php
    session_start(); 
    require_once(ADMIN_DIR."/Libraries/dbConnect.php");
    require_once(ADMIN_DIR."/Libraries/ui_elements.php");

    /**
     * Display different reports for statisticle purposes
     */
    class Reports {
        private $userID;
        function __construct(){
            $this->userID = $_SESSION['userID'];
        }

        function init(){
            echo "<h1 style='text-align:center; margin:40px;'>Reports</h1><hr>";
            if(isset($_POST['ClientCallOut']) && $_POST['ClientCallOut']=="View"){
                $this->clientCallOutReport($this->userID);
            }else if(isset($_POST['LatestCallOut']) && $_POST['LatestCallOut']=="View"){
                $this->latestCallOutReport($this->userID);
            }else{
                $this->displayreports($this->userID);
            }
            
            
        }

        function displayreports($userID){
            
            echo "<br><table style='width: 100%'>";
            echo "<tr>";
                echo "<td style='width: 50%'><div class='card'>
                        <div class='card-body'>
                            <form method='post'>
                                <h5 class='card-title'>Latest Callout Per Client
                                    <input type='submit' name='LatestCallOut' id='LatestCallOut' class='btn btn-info' style='float:right;' value='View'>
                                </h5>
                            </form>  
                        </div>
                    </div>
                    </td>
                    <td style='width: 50%'><div class='card'>
                        <div class='card-body'>
                            <form method='post'>
                                <h5 class='card-title'>Client Callouts for Current Month
                                    <input type='submit' name='ClientCallOut' id='ClientCallOut' class='btn btn-info' style='float:right;' value='View'>
                                </h5>
                            </form>    
                        </div>
                    </div>
                    </td>";
            echo "</tr>";
            echo "</table>";

        }

        function latestCallOutReport($userID){
            $headings = getTableColumns("timesheets","id,comments,dateCreated");
            $sql = "SELECT t.id, c.company as clientID, MAX(t.date) as date, t.total FROM timesheets t 
                    LEFT JOIN clients c ON c.id=t.clientID 
                    WHERE c.userID = '$userID'
                    GROUP BY c.id
                    ORDER BY t.date DESC";

            $result = exeSQL($sql);

            echo "<table class='table table-striped'>";
            echo "<thead>";
            echo "<th>#</th>";
            foreach($headings as $heading){
                $heading = ucwords(str_replace("_"," ",$heading['Column']));
                echo "<th>$heading</th>";
            }
            echo "</thead>";
            $cnt = 1;
            foreach($result as $r){
                echo "<tr>";
                echo "<td>$cnt</td>";
                foreach($headings as $heading){
                    echo "<td>{$r[$heading['Column']]}</td>";
                }
                echo "</tr>";
                $cnt++;
            }
            echo "</table>";
        }
       

        function clientCallOutReport($userID){
            echo "<div class='alert alert-info'>Under Construction</div>";
        }
    }
?>