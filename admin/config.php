<?php
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(E_ALL);
    
    /*=====[DB Parameters]=====*/
    define("DB_HOST", "localhost");
    define("DB_USER", "root");
    define("DB_PASS", "");
    define("DB_NAME", "timesheets");
    /*==========*/

    // App Root
    define("APPROOT", dirname(dirname(__FILE__)));
    // URL Root
    define("URLROOT", "http://localhost/timesheets");
    // Site Name
    define("SITENAME", "TimeSheets");
    //Admin Root
    define("ADMIN_DIR", APPROOT."/admin");
    //Modules Root
    define("MOD_DIR", ADMIN_DIR."/Modules");

    // App Version
    define("APPVERSION", "1.0.0");
?>